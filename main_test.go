package main

import (
	"bufio"
	"bytes"
	"os/exec"
	"strings"
	"testing"
)

func TestDecodeControlChars(t *testing.T) {
	var cases = []struct {
		input    string
		expected string
		offset   int
	}{
		//single-byte encoding
		{`\x61utomatic`, "a", 4},
		// multi-byte encoding
		{`\xe4\xb8\x96test`, "世", 12},
		// control character
		{`\x14 or not`, `\\x14`, 4},
		// invalid hex
		{`\xhltest`, `\\xhl`, 4},
		// undersized string
		{`\xa`, `\\xa`, 3},
		// A computer still using  ISO-8859-1...
		{`\xe9tat`, `\\xe9`, 4},
	}
	for _, testCase := range cases {
		converted := []byte(testCase.input)
		output, offset := decodeControlChars(converted, len(testCase.input))
		if testCase.expected != string(output) {
			t.Errorf("expected '%v', got '%v'", []byte(testCase.expected), output)
		}
		if offset != testCase.offset {
			t.Errorf("expected offset %d, got %d", testCase.offset, offset)
		}
	}
}

func TestRemoveControlChars(t *testing.T) {
	var testCases = []struct {
		input, expected string
	}{
		// single-byte substitution in the middle of a sentence
		{`{"msg": "some de\x61th-bound thing"}`, `{"msg": "some death-bound thing"}`},
		// no change
		{"this is a simple test with no modifications", "this is a simple test with no modifications"},
	}
	for _, testCase := range testCases {
		byteStr := []byte(testCase.input)
		result := removeControlChars(byteStr)
		// remove trailing null bytes. We don't care when passing our input to the io.Writer but here they count!
		asString := string(bytes.Trim(result, "\x00"))
		if asString != testCase.expected {
			t.Errorf("expected '%v', got '%v'", testCase.expected, asString)
		}
	}
}

func TestGloggerRun(t *testing.T) {
	var testCases = []struct {
		input, expected string
	}{
		// T368640
		{`wiki/O\xc2\x9clepia\xc4\x87`, `wiki/O\\xc2\\x9clepiać`},
		{"this is a foobar\nand this is an \x61utomation", "this is a foobar\nand this is an automation"},
		{"Simple line with no changes", "Simple line with no changes"},
		{
			`{"ecs.version": "1.11.0", "event.category": ["network", "web"], "event.dataset": "apache.access", "event.duration": "16142", "event.kind": "event", "event.outcome": "unknown", "event.type": ["access", "connection"], "http.request.headers.accept_language": "-", "http.request.headers.x_analytics": "-", "http.request.headers.x_client_ip": "-", "http.request.headers.x_forwarded_for": "1.2.3.4", "http.request.headers.x_request_id": "no-label", "http.request.method": "GET", "http.request.referrer": "-", "http.response.bytes": "500", "http.response.headers.content_type": "text/html", "http.response.headers.user": "-", "http.response.status_code": "200", "labels.handler": "proxy:unix:/run/shared/fpm-www.sock|fcgi://localhost", "labels.httpd_server_name": "wikipedia", "server.ip": "127.0.0.1", "service.type": "apache2", "source.ip": "127.0.0.1", "timestamp": "2020-03-09T05:40:02", "url.domain": "zh.wikipedia.org", "url.full": "http://zh.wikipedia.org/wiki/\xe4\xba\x94\xe6\x9c\x88\xe5\x88\x9d\xe4\xb8\x83", "url.path": "/wiki/\xe4\xba\x94\xe6\x9c\x88\xe5\x88\x9d\xe4\xb8\x83", "url.query": "", "user.name": "-", "user_agent.original": "Mozilla/5.0 (compatible; FooBot/1.0; +http://example.com/robot/)"}`,
			`{"ecs.version": "1.11.0", "event.category": ["network", "web"], "event.dataset": "apache.access", "event.duration": "16142", "event.kind": "event", "event.outcome": "unknown", "event.type": ["access", "connection"], "http.request.headers.accept_language": "-", "http.request.headers.x_analytics": "-", "http.request.headers.x_client_ip": "-", "http.request.headers.x_forwarded_for": "1.2.3.4", "http.request.headers.x_request_id": "no-label", "http.request.method": "GET", "http.request.referrer": "-", "http.response.bytes": "500", "http.response.headers.content_type": "text/html", "http.response.headers.user": "-", "http.response.status_code": "200", "labels.handler": "proxy:unix:/run/shared/fpm-www.sock|fcgi://localhost", "labels.httpd_server_name": "wikipedia", "server.ip": "127.0.0.1", "service.type": "apache2", "source.ip": "127.0.0.1", "timestamp": "2020-03-09T05:40:02", "url.domain": "zh.wikipedia.org", "url.full": "http://zh.wikipedia.org/wiki/五月初七", "url.path": "/wiki/五月初七", "url.query": "", "user.name": "-", "user_agent.original": "Mozilla/5.0 (compatible; FooBot/1.0; +http://example.com/robot/)"}`,
		},
		{`This is a line from a computer with broken encoding: \xe9tat`, `This is a line from a computer with broken encoding: \\xe9tat`},
		{`This is a line from a computer with broken \xe9 encoding and spaces`, `This is a line from a computer with broken \\xe9 encoding and spaces`},
	}
	for _, testCase := range testCases {
		expected := testCase.expected
		input := testCase.input
		scanner := bufio.NewScanner(strings.NewReader(input))
		// Let's log to stdout, and collect it.
		logger := exec.Command("/bin/bash", "-c", "/usr/bin/echo \"$(</dev/stdin)\"")
		stdout, err := logger.StdoutPipe()
		if err != nil {
			t.Errorf("Could not open the stdoutpipe to echo: %v", err)
		}
		g := NewGlogger(scanner, logger)
		go g.Run()
		result := make([]byte, len([]byte(expected)))
		buflen, err := stdout.Read(result)
		if err != nil {
			t.Errorf("%v", err)
		}
		resStr := string(result[:buflen])
		if resStr != expected {
			t.Errorf("expected '%s', got '%s'", expected, resStr)
		}
	}
}

// Benchmark for our filtering function
// We test:
// - multiple decodings: Almost worst-case scenario (but also - real world scenario) - with multiple multibyte characters encoded in sequence.
// - no decoding zero-alloc: Best-case scenario (should be 99% of logs more or less)
func BenchmarkRemoveControlChars(b *testing.B) {
	var benchmarks = []struct {
		label string
		input []byte
	}{
		{
			"multiple decodings",
			[]byte(`{"ecs.version": "1.11.0", "event.category": ["network", "web"], "event.dataset": "apache.access", "event.duration": "16142", "event.kind": "event", "event.outcome": "unknown", "event.type": ["access", "connection"], "http.request.headers.accept_language": "-", "http.request.headers.x_analytics": "-", "http.request.headers.x_client_ip": "-", "http.request.headers.x_forwarded_for": "1.2.3.4", "http.request.headers.x_request_id": "no-label", "http.request.method": "GET", "http.request.referrer": "-", "http.response.bytes": "500", "http.response.headers.content_type": "text/html", "http.response.headers.user": "-", "http.response.status_code": "200", "labels.handler": "proxy:unix:/run/shared/fpm-www.sock|fcgi://localhost", "labels.httpd_server_name": "wikipedia", "server.ip": "127.0.0.1", "service.type": "apache2", "source.ip": "127.0.0.1", "timestamp": "2020-03-09T05:40:02", "url.domain": "zh.wikipedia.org", "url.full": "http://zh.wikipedia.org/wiki/\xe4\xba\x94\xe6\x9c\x88\xe5\x88\x9d\xe4\xb8\x83", "url.path": "/wiki/\xe4\xba\x94\xe6\x9c\x88\xe5\x88\x9d\xe4\xb8\x83", "url.query": "", "user.name": "-", "user_agent.original": "Mozilla/5.0 (compatible; FooBot/1.0; +http://example.com/robot/)"}`),
		},
		{
			"no decoding zero-alloc",
			[]byte(`{"ecs.version": "1.11.0", "event.category": ["network", "web"], "event.dataset": "apache.access", "event.duration": "16142", "event.kind": "event", "event.outcome": "unknown", "event.type": ["access", "connection"], "http.request.headers.accept_language": "-", "http.request.headers.x_analytics": "-", "http.request.headers.x_client_ip": "-", "http.request.headers.x_forwarded_for": "1.2.3.4", "http.request.headers.x_request_id": "no-label", "http.request.method": "GET", "http.request.referrer": "-", "http.response.bytes": "500", "http.response.headers.content_type": "text/html", "http.response.headers.user": "-", "http.response.status_code": "200", "labels.handler": "proxy:unix:/run/shared/fpm-www.sock|fcgi://localhost", "labels.httpd_server_name": "wikipedia", "server.ip": "127.0.0.1", "service.type": "apache2", "source.ip": "127.0.0.1", "timestamp": "2020-03-09T05:40:02", "url.domain": "zh.wikipedia.org", "url.full": "http://zh.wikipedia.org/wiki/Australia", "url.path": "/wiki/Australia", "url.query": "", "user.name": "-", "user_agent.original": "Mozilla/5.0 (compatible; FooBot/1.0; +http://example.com/robot/)"}`),
		},
	}
	for _, benchmark := range benchmarks {
		b.Run(benchmark.label, func(b *testing.B) {
			for i := 0; i < b.N; i++ {
				removeControlChars(benchmark.input)
			}
		})
	}
}
