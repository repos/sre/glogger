package main

/*
	Simple apache 2 rsyslog logger which allows to send proper json by
	re-converting the escaped utf-8 sequences like \xNN to the correct
	bytestrings.

	Copyright (c) 2023 Giuseppe Lavagetto, Wikimedia Foundation

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
import (
	"bufio"
	"bytes"
	"encoding/hex"
	"io"
	"os"
	"os/exec"
	"unicode/utf8"
)

const (
	backslash byte = 92
	x         byte = 120
)

// Find the full sequence to escape.
func findEscape(line []byte, size int) (toEscape []byte, length int) {
	length = 0
	i := 0
	toEscape = make([]byte, size)
	for pos := 0; pos <= size-4; pos += 4 {
		if line[pos] == backslash && line[pos+1] == x {
			toEscape[i] = line[pos+2]
			toEscape[i+1] = line[pos+3]
			i += 2
			length += 4
		} else {
			break
		}
	}
	return
}

// Escape the backslashes in the line. Try to do it with a single allocation.
func escapeSlash(line []byte) []byte {
	result := make([]byte, len(line)*2)
	i := 0
	for _, char := range line {
		if char == backslash {
			result[i] = backslash
			i++
		}
		result[i] = char
		i++
	}
	return result[:i]
}

// translate the \xNN notation to a byte of value "NN"
// We also try to check that the resulting rune is printable (TODO)
func decodeControlChars(line []byte, size int) (deEscaped []byte, offset int) {
	escaped, offset := findEscape(line, size)
	// Check for an incomplete escape sequence returned
	escapeLen := offset / 2
	if escapeLen == 0 || escapeLen%2 != 0 {
		if len(line) < 4 {
			offset = len(line)
		} else {
			offset = 4
		}
		// Again, we need to escape the backslash
		deEscaped = escapeSlash(line[:offset])
		return
	}
	// offset is 4 for each escape sequence, which is one single byte
	deEscaped = make([]byte, offset/4)
	_, err := hex.Decode(deEscaped, escaped[:escapeLen])
	// If we couldn't decode the byte sequence, just add it to the result
	// but properly escaped
	if err != nil {
		deEscaped = escapeSlash(line[:offset])
		return
	}
	// Now check we can get a proper rune out of the escaped sequence
	unicodeValue, _ := utf8.DecodeRune(deEscaped)
	if unicodeValue == utf8.RuneError {
		// If the byte isn't proper utf-8, we just add a second \ to the byte sequence
		// This should account for characters encoded in various other encodings.
		deEscaped = escapeSlash(line[:offset])
		return
	}
	// Check for utf8 control characters
	// see https://en.wikipedia.org/wiki/Unicode_control_characters
	if (unicodeValue <= '\u001F') || (unicodeValue == '\u007F') || (unicodeValue >= '\u0080' && unicodeValue <= '\u009C') {
		deEscaped = escapeSlash(line[:offset])
	}
	return
}

type GloggerFilterFunc func([]byte) []byte

// scan a bytestring for \xHH; then transform it to its corresponding rune
// This should be O(N) with N the length of the string.
func removeControlChars(line []byte) (result []byte) {
	// First check if there's any escape sequence at all:
	firstBackslash := bytes.IndexByte(line, backslash)
	if firstBackslash < 0 {
		return line
	}
	size := len(line)
	lastEscapePos := size - 4
	// try to avoid allocations. Please note: a string with no escaped
	// characters will be as long as the original, any with escaped characters will be shorter as
	// \xNN (4 bytes) becomes the single byte NN
	result = make([]byte, size)
	copy(result, line[:firstBackslash])
	var resultidx int = firstBackslash
	for pos := firstBackslash; pos < size; pos++ {
		char := line[pos]
		if char == backslash && pos <= lastEscapePos && line[pos+1] == x {
			// find the escaped sequence
			decoded, offset := decodeControlChars(line[pos:], size-pos)
			newIdx := resultidx + len(decoded)
			// If the decoded sequence is longer than the original, we need add one element to the end of the slice
			sizeDiff := newIdx - resultidx - offset
			if sizeDiff > 0 {
				toAppend := make([]byte, sizeDiff)
				result = append(result, toAppend...)
			}
			copy(result[resultidx:newIdx], decoded)
			// We do increment by one above
			pos += offset - 1
			resultidx = newIdx
		} else {
			result[resultidx] = char
			resultidx++
		}
	}
	return
}

// Glogger is the container of the control stucture
// allowing to read from a scanner and piping the input,
// after filtering, to a logger program's stdin
type Glogger struct {
	scanner    *bufio.Scanner
	logger     *exec.Cmd
	filterFunc GloggerFilterFunc
}

// SetFilterFunction sets a different filter function
func (g *Glogger) SetFilterFunction(f GloggerFilterFunc) {
	g.filterFunc = f
}

// Run executes the program
func (g *Glogger) Run() error {
	w, err := g.logger.StdinPipe()
	if err != nil {
		return err
	}
	defer g.shutdown(w)
	if err = g.logger.Start(); err != nil {
		return err
	}
	for g.scanner.Scan() {
		line := g.scanner.Bytes()
		data := g.filterFunc(line)
		_, err := w.Write(data)
		if err != nil {
			panic(err)
		}
		w.Write([]byte("\n"))
	}
	return nil
}

func (g *Glogger) shutdown(w io.WriteCloser) {
	// Close the input, then wait for the logger to exit
	w.Close()
	g.logger.Wait()
}

// Sets up a new glogger instance with the default filtering function
func NewGlogger(scanner *bufio.Scanner, logger *exec.Cmd) *Glogger {
	return &Glogger{scanner, logger, removeControlChars}
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	logger := exec.Command("/usr/bin/logger", os.Args[1:]...)
	g := NewGlogger(scanner, logger)
	if err := g.Run(); err != nil {
		panic(err)
	}
}
