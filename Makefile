# This makefile is mostly intended as help for local development.
.PHONY: test benchmark profileenv profile memprofile
test:
	go test -v

benchmark:
	go test -bench=. -benchmem $$PROFILEOPTS

profileenv:
	export PROFILEOPTS="-memprofile memprofile.out -cpuprofile profile.out"

profile: profileenv benchmark
	go tool pprof profile.out

memprofile: profileenv benchmark
	go tool pprof memprofile.out
