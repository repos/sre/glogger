# Glogger

This program does a very simple thing:
* Accept input
* Un-escape `\xNN` sequences that are valid UTF-8 that isn't part
  of the control characters ranges
* Pass this filtered input to `logger(1)` to relay it to a
  local or remote syslog instance

It has been designed for overcoming the issues emitting json-formatted
logs from apache's httpd. See https://phabricator.wikimedia.org/T340935
for some of the gory details.

### Performance
This program needs to be very fast. I have benchmarked its core escaping function to
run in 0.03 microseconds when no escape happens, and 1.0 microseconds when multiple
escapes are required, on my 6 years old hardware. This is more than adequate and I
decided we don't really need further optimizations - in fact profiling the code shows
that most time is spent in optimized assembly in `bytes.IndexByte`.